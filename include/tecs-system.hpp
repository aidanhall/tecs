#ifndef TECS_SYSTEM_H
#define TECS_SYSTEM_H

#include "tecs.hpp"
#include <span>

namespace Tecs {
struct SingleEntitySetSystem {
  using Function = void(Coordinator &, const std::unordered_set<Entity> &);

  Function* run;
};

struct MultipleEntitySetSystem {
  using Function = void(Coordinator &,
                        const std::span<const std::unordered_set<Entity>>);
  Function* run;
};

struct PerEntitySystem {
  using Function = void(Coordinator &, const Entity);
  Function* run;
};

struct InterestedClient {
  InterestedId id;
};

void registerSystemComponents(Coordinator &);
void runSystems(Coordinator &, const InterestedId);

InterestedId makeSystemInterest(Coordinator &, const ComponentMask &mask,
                                const ComponentMask &exclude = {});
} // namespace Tecs

#endif /* TECS_SYSTEM_H */
