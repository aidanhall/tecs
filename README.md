# Tiny ECS

Based on Austin Morlan's blog post:

https://austinmorlan.com/posts/entity_component_system/

All components of a type are stored in one big vector,
with space allocated for every entity to have every component
(i.e. in a poorly-optimised way).

## Plans for Development

* Further automate stuff like running `TimedSystem` systems.
* Actually control access to procedures. E.g. Prevent systems from
  calling `destroyEntity` and `destroyQueued`.
* Either make a better `System` inheritance tree or turn systems into
  entities with components for running them.
* Improved `System` creation and registration workflow.
* "Meta"-components: Collections of components that are added
  together. E.g. `Position`, `Rotation` and `Velocity`.  Probably
  unnecessary, since we can either use one big component or manually
  add all of them.
* Component dependencies: Explicitly specifying a list of other
  components required for an entity to have another.
  No functional benefit, but it could aid the programmer in detecting
  and preventing bugs related to an entity not having a component.
* Managed game loop:
  * Tick and delta variables
  * De-coupled rendering and logic (and physics?) loops.
* Scenes:  These could possibly be implemented as a separate module of
  the hypothetical game engine that is to come.
* Entity Hierarchy.
* Multiple System Signatures: Allow a system to operate on multiple
  sets of entities with different components.
* "Flag" components with no actual data.

## Entity Hierarchy
Depending on the performance characteristics, this could almost
certainly be implemented using components; either `Children` or
`Parent`.  The latter would obviously be simpler.  The big issue is
that this is a mechanism for *persistent inter-entity references*,
opening the can of worms of reference invalidation.  However, this
isn't likely to be an issue with strict hierarchical references, where
children typically have a lifetime that cannot exceed that of the
parent.

Perhaps if we consider entity hierarchies as another composition
mechanism, then we can debate if they are even necessary.  The answer
is probably that they are.

## System Components
I currently have `System` and `TimedSystem`, but within just `System`
I have two options for defining its behaviour: `runEntity` for the
simple (yet common) case of a `System` that only needs to iterate over
all the relevant entities independently, and `run` for more manual
specification.

It's also possible I could end up wanting another type of system like
`TimedSystem`, and then also wanting a system with the additional
attributes of both.  Having already put the work in to bootstrap such
an effective composition abstraction as an ECS, applying it to
building systems seems fairly reasonable.

### Issues
* Likelihood of using function pointers, which may introduce
  inefficiencies(?).
* Making it harder to have a granular specification of the order
  systems run.
  
This second one is a lot more concrete, since, for example, some of my
systems have to run during a "drawing" phase of the game loop, which
it would probably be ideal to minimise.  Naively, this could be solved
by having separate physics, drawing and "unbounded" system components,
which would be handled differently, similarly to `FixedUpdate()` and
`Update()` in *Unity*.

### Design
What I want:
* 1 component for the original SingleEntitySetSystem type;
* 1 component for the MultipleEntitySetSystem type;
* 1 component for the PerEntitySystem type.

These need to be handled differently, so of course there should be
different code for each.

I can de-couple system signatures from systems: We just need to
maintain the set of relevant entities for each signature, and then
have a way to refer to all the sets relevant to a system when it is
run.  The potential many-to-one relationship between system signatures
and systems either suggests signatures should be managed outside the
ECS (as they have been), or that some generalisation is in order.

Centralised storage of the signatures makes it easier to keep the
entity sets up to date, as well as making it more straightforward to
reuse/adapt the existing `SystemManager` code.  It could be re-branded
to reflect its changed use.

One nice source of power is that the same function could be used in
multiple systems or "system executors", using different
parameters/signatures to change the behaviour.  Concretely: A function
for running per-entity systems could be used for graphical and physics
system runners.

More support for "ad-hoc" Component mask/Signature queries could be
powerful.  E.g. One-shot operation on all Entities meeting some
criteria.

#### System Executors

These are manually called in the game loop at the appropriate points
to run certain sets of systems.  They could themselves be entities, or
possibly just a function and a `Signature`.

## Flag Components
These would take up a spot in the component mask but would not contain
actual data.  This could be useful for a marker like "bloodied", but
would detract from the "beautiful" simplicity of the system.
